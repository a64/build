
# Build for pinebook

Be aware you will need ~ 150 gB to build /e/
```
mkdir e
cd e
repo init -u https://gitlab.e.foundation/e/os/android.git -b v1-nougat
git clone https://gitlab.e.foundation .repo/local_manifests
repo sync
```

in case of failure while downloading

```
repo sync -c -f -j2 --force-sync --no-clone-bundle --no-tags
```


Prepare the build

```
export USE_CCACHE=1
export WITH_SU=true
prebuilts/misc/linux-x86/ccache/ccache -M 50G
source build/envsetup.sh
```
Then start the build

```
lunch lineage_tulip_chiphd_pinebook-userdebug
LC_ALL=C make -j4 && sdcard_image pine64_e.img.gz pinebook
```



